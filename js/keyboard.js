;(function(exports){
	var KeyBoard = function(callback){
		
		var body = document.getElementsByTagName('body')[0];
		var DIV_ID = "keyboarddiv";
		//if(document.activeElement.type!="text")
			//return;
		//if(document.getElementById(DIV_ID)){
		//	body.removeChild(document.getElementById(DIV_ID));
		//}
		
		//this.input = document.activeElement;
		this.input =document.getElementById("actualprice");
		this.el = document.createElement('div');
		
		//var self = this;
		var self = document.getElementById("actualprice");
		var zIndex = 1;
		var width =  '400px';
		
		var height =  '500px';
		var fontSize =  '15px';
		var backgroundColor =  '#666666';
		var TABLE_ID =  'table_0909099';
		var mobile = typeof orientation !== 'undefined';
		
		this.el.id = DIV_ID;
		this.el.style.position = 'absolute';
		this.el.style.left = "500px";
		this.el.style.top = "80px";
		//this.el.style.right = 0;
		//this.el.style.bottom = 600;
		this.el.style.zIndex = zIndex;
		this.el.style.width = width;
		this.el.style.height = "210px";
		this.el.style.backgroundColor = "#999999";
		
		//样式
		var cssStr = '<style type="text/css">';
		cssStr += '#' + TABLE_ID + '{text-align:center;width:100%;height:60px;background-color:#999999;border:1px solid #D3D9DF;}';
		cssStr += '#' + TABLE_ID + ' td{width:25%;border:1px solid #D3D9DF;background-color:#FFFFFF;border-radius:0px;font:25px "微软雅黑", "Verdana";}';
		if(!mobile){
			cssStr += '#' + TABLE_ID + ' td:hover{background-color:#1FB9FF;color:#FFF;}';
		}
		cssStr += '</style>';
		
		//Button
		//var btnStr = '<div style="width:60px;height:28px;background-color:#1FB9FF;';
		//btnStr += 'float:right;margin-right:5px;text-align:center;color:#fff;';
		//btnStr += 'line-height:28px;border-radius:3px;margin-bottom:5px;cursor:pointer;">完成</div>';
		
		//table
		var tableStr = '<table id="' + TABLE_ID + '" border="1" cellspacing="0px" cellpadding="0px">';
			tableStr += '<tr><td height="70px" >1</td><td>2</td><td>3</td><td>4</td></tr>';
			
			tableStr += '<tr><td height="70px" >5</td><td>6</td><td>7</td><td>8</td></tr>';
			tableStr += '<tr><td>9</td><td>0</td>';
			tableStr += '<td height="70px">.</td>';
			tableStr += '<td height="70px">删除</td></tr>';
		//	tableStr += '<td  colspan="2">完成</td></tr>';
			tableStr += '</table>';
		this.el.innerHTML = cssStr  + tableStr;
		
		function addEvent(e){
			var ev = e || window.event;
			var clickEl = ev.element || ev.target;
			var value = clickEl.textContent || clickEl.innerText;
			if(clickEl.tagName.toLocaleLowerCase() == 'td' && value !== "删除"&& value != "完成"){
				keyboardtip(value);
				if(self){
					self.value += value;
				}
			}else if(clickEl.tagName.toLocaleLowerCase() == 'td' && value == "完成"){
				
				
				
				//body.removeChild(self.el);
				if(callback) callback();
			}else if(clickEl.tagName.toLocaleLowerCase() == 'td' && value == "删除"){
				var num = self.value;
				if(num){
					var newNum = num.substr(0, num.length - 1);
					self.value = newNum;
				}
			}
		}
		
		if(mobile){
			this.el.ontouchstart = addEvent;
		}else{
			this.el.onclick = addEvent;
		}
		body.appendChild(this.el);
	}
	
	exports.KeyBoard = KeyBoard;
	

})(window);

function hiddenkeyboard()
{
	var body = document.getElementsByTagName('body')[0];
	var DIV_ID="keyboarddiv";
	if(document.getElementById(DIV_ID)){
		body.removeChild(document.getElementById(DIV_ID));
	}
}