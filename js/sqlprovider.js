// JavaScript Document
var  sqlerrorHandler =  function(tx,e)
	  {     
	
	    console.error(e.message);      
	  }		
		

var SQLProvider = Class.create();  
SQLProvider.prototype = {  
   
    Init:function(options,size){  
	    this.Document = document;
         
        this.SetOptions(options);//设置参数  
      
		this.dbName = this.Options.dbName;
	  
		var db = openDatabase(this.dbName, '1.0', 'database for ' + this.dbName, (size || 2) * 1024 * 1024);      
	    this.db = db;   
		
		if(!db)
		{ this.Document.write("not support web sql");}
    },  
	
	//设置参数  
    SetOptions:function(options){  
        this.Options={  
		    dbName:"ZTTeaShop",
        };  
        Extend(this.Options,options||{});  
    },  
	   
	 createTable : function (sql,callBack) 
	  {           
	       
			
		    this.db.transaction(function (tx) {            
			   tx.executeSql(sql, [], 
			   function (tx) {   if (callBack) {callBack();}
			             }, 
			   sqlerrorHandler);           });
			 
			
      },
	  
	   //
	  loadTable : function (sql,values, callback)
	   {           this.db.transaction(function (tx) {             
			tx.executeSql(sql, values, function (tx, result) {                 
			  if (callback) callback(result); //使用回调                      
					   }, sqlerrorHandler);           });     
	   },
	 
	   //
	   insertMultiRows : function(tableName,fields,values, callback){
		    
		    this.db.transaction(function (tx) { 
			for(var i = 0;i<values.length;i++)
			{
			     var sql = "INSERT INTO " + tableName + " (" + fields.join(",") + ") values ("   
			          + new Array(values[i].length + 1).join(",?").substr(1)+")"; 
				 var ary = values[i];
				 tx.executeSql(sql, values[i], function (tx) { });  	   
			}
			if(callback) callback();
			 },function(e){console.error("aaa"+e.message);});
	   },
	     //
	  insertRow : function (tableName, fields, values, callback) {    
	  
	       
		      var sql = "INSERT INTO " + tableName + " (" + fields.join(",") + ") values ("   
			          + new Array(values.length + 1).join(",?").substr(1)+")";   
		  
			 this.db.transaction(function (tx) {   
			     
				 tx.executeSql(sql, values, function (tx) { 
				    tx.executeSql("SELECT max(id) id from " + tableName, [], 
						function (tx, result)
						 {    
						 
							var item = result.rows.item(0);         
							var id = parseInt(item.id);
						   
							var sqllog = "INSERT#"+tableName+"#"+["id"].concat(fields).join(",")+"#"+["'"+id].concat(values).join("','")+"'";
                            if (callback) callback(id);
			              
							  });  });
				 			     	        
							   
				    } ,function(e){console.error("aaa"+e.message);});
					 
	 } ,

     //											  
      deleteRow :function (tableName,id,callback) 
	  {          
	    
		    var sql = "DELETE FROM " + tableName + " WHERE id"  + " = ?";   
		    var sqllog = "DELETE#"+tableName+"#id"+"#"+id;
		    this.db.transaction(function (tx) {             
		    tx.executeSql(sql, [id], function(){ if (callback) callback(); //使用回调 
						        });    
		   
						  },function(e){console.error("aaa"+e.message);});     
	  } ,
	  //
	   updateMultiRows : function (tableName, fields, values,callback)
	   {      
	       
	       var len = fields.length;          
           this.db.transaction(function (tx) {  
		   for(var j=0;j<values.length;j++)
		   {       
		       var sql = "";    
		         for (i = 1; i < len; i++) 
		         {            
			      if (i == 1) sql += fields[i] + " = '" + values[j][i] + "'";           
				  else sql += "," + fields[i] + " = '" + values[j][i] + "'";           
		         }    
				        
		   sql = 'UPDATE ' + tableName + ' SET ' + sql + ' where ' + fields[0] + '= ?';           //log.debug("sql:" + sql);
		   // var sqllog = "UPDATE#"+tableName+"#"+fields.join(",")+"#"+values[j].join(",");
			 var sqllog = "UPDATE#"+tableName+"#"+fields.join(",")+"#"+"'"+values[j].join("','")+"'";
				tx.executeSql(sql, [values[j][0]],  function(){  if (callback) callback(); 
				 });          
				             
		   }},function(e){console.error("aaa"+e.message);});       
                   
	  } ,
	  //execute one statement
	  executestat:function(sql,callback)
	  {
		  
			  var sqllog = sql;

           this.db.transaction(function (tx) {            
				tx.executeSql(sql, [],  function(){  
				 });  
				
										
	           
				             },function(e){console.error("aaa"+e.message);});       
                   
	  
	  
	  },
	  //
	   updateRow : function (tableName, fields, values,id,callback)
	   {      
	    
	       var len = fields.length;           
		   var sql = "";         
		   for (i = 0; i < len; i++) 
		   {            
			      if (i == 0) sql += fields[i] + " = '" + values[i] + "'";           
				  else sql += "," + fields[i] + " = '" + values[i] + "'";           
		   }         
		   sql = 'UPDATE ' + tableName + ' SET ' + sql + ' where id= ?';           //log.debug("sql:" + sql);
		   var sqllog = "UPDATE#"+tableName+"#"+fields.join(",")+"#"+"'"+values.join("','")+"'";

           this.db.transaction(function (tx) {            
				tx.executeSql(sql, [id],  function(){      if (callback) callback();
				 });  
			
	           
				             },function(e){console.error("aaa"+e.message);});       
                   
	  } 
	  
	  
	 
   
};  